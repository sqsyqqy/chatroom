#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <network.h>
#include "threadpool.h"

#define MAX_CLIENT (50)
#define BUF_SIZE (4096)

Network* client[MAX_CLIENT];

// 添加函数
int add_client(Network* nw)
{
	for(int i=0; i<MAX_CLIENT; i++)
	{
		if(NULL == client[i])
		{
			client[i] = nw;
			return i;
		}
	}
}

// 向其他所有用户发送消息函数
void send_client(int index,char* buf)
{
	for(int i=0; i<MAX_CLIENT; i++)
	{
		if(NULL != client[i] && i != index)
		{
			send_nw(client[i],buf,strlen(buf)+1);	
		}
	}
}

// 服务客户端的业务逻辑函数 // arg相当于cli_nw
void enter(void* arg)
{
	// 把arg添加到客户端数组
	int index = add_client(arg);

	char* buf = malloc(BUF_SIZE);

	// 接收昵称
	int ret = recv_nw(client[index],buf,BUF_SIZE);
	if(0 >= ret)
	{
		puts("连接失败！");
		close_nw(client[index]);
		client[index] = NULL;
		free(buf);
		return;
	}

	strcat(buf," 进入聊天室");
	send_client(index,buf);

	buf[ret-1] = ':';
	char* msg = buf + ret;

	for(;;)
	{
		ret = recv_nw(client[index],msg,BUF_SIZE-(msg-buf));
		if(0 >= ret || 0 == strcmp(msg,"quit"))
		{
			sprintf(msg,"已经退出聊天室！");
			send_client(index,buf);
			close_nw(client[index]);
			client[index] = NULL;
			free(buf);
			return;
		}
		send_client(index,buf);
	}
}

int main(int argc,const char* argv[])
{
	// 检查命令行参数
	if(3 != argc)
	{
		puts("User: ./chat_server <ip> <port>");
		return EXIT_FAILURE;
	}

	// 创建线程池
	ThreadPool* thread = create_threadpool(MAX_CLIENT,20,enter);
	// 开启线程池
	start_threadpool(thread);

	// 启动网络通信
	Network* srv_nw = init_nw(SOCK_STREAM,atoi(argv[2]),argv[1],true);
	if(NULL == srv_nw)
	{
		puts("网络错误请检查！");
		return EXIT_FAILURE;	
	}

	for(;;)
	{
		// 等待客户端连接
		Network* cli_nw = accept_nw(srv_nw);
		if(NULL == cli_nw)
		{
			puts("网络连接异常！");
			return EXIT_FAILURE;
		}

		// 添加到仓库
		push_threadpool(thread,cli_nw);
	}
}
