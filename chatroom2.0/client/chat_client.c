#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <network.h>
#include <pthread.h>

char buf[4096];
size_t buf_size = sizeof(buf);

void* run(void* arg)
{
	Network* nw = arg;
	for(;;)
	{
		int ret = recv_nw(nw,buf,buf_size);	
		if(0 >= ret)
		{
			puts("服务器正在升级，请稍后...");
			close_nw(nw);
			exit(0);
		}
		printf("\r%s\n",buf); // 回到行首
		printf(">>>");
		fflush(stdout);
	}
}

int main(int argc,const char* argv[])
{
	if(3 != argc)
	{
		puts("User: ./client <ip> <port>");
		return EXIT_FAILURE;
	}

	Network* nw = init_nw(SOCK_STREAM,atoi(argv[2]),argv[1],false);
	if(NULL == nw)
	{
		puts("网络连接失败！");
		return EXIT_FAILURE;
	}

	// 发送昵称
	printf("请输入你的昵称：");
	scanf("%s",buf);
	send_nw(nw,buf,strlen(buf)+1);

	// 创建专门接受其他用户发送消息的线程
	pthread_t tid;
	pthread_create(&tid,NULL,run,nw);

	for(;;)
	{
		printf(">>>");
		stdin->_IO_read_ptr = stdin->_IO_read_end;
		gets(buf);
		int ret = send_nw(nw,buf,strlen(buf)+1);
		if(0 >= ret)
		{
			puts("服务器正在升级，请稍后...");
			close_nw(nw);
			return EXIT_FAILURE;
		}
		if(0 == strcmp(buf,"quit"))
		{
			puts("您已退出聊天室！");
			close_nw(nw);
			return EXIT_FAILURE;
		}
	}
}
