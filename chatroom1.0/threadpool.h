#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <pthread.h>
#include "queue.h"

// 线程执行的真正的业务逻辑函数格式
typedef void (*EnterFP)(void*);

typedef struct ThreadPool
{
	int thread_cnt;		// 线程数量
	pthread_t* tids;	// 线程ID
	Queue* store;		// 队列仓库
	pthread_mutex_t flock;	// 队头互斥锁
	pthread_mutex_t rlock;	// 队尾互斥锁
	pthread_cond_t empty;	// 空仓的条件变量
	pthread_cond_t full;	// 满仓的条件变量
	EnterFP enter;		// 线程的业务逻辑函数
}ThreadPool;

// 创建线程池
ThreadPool* create_threadpool(int thread_cnt,int store_cnt,EnterFP enter);

// 启动线程池
void start_threadpool(ThreadPool* thread);

// 生产数据
void push_threadpool(ThreadPool* thread,void* data);

// 消费数据
void* pop_threadpool(ThreadPool* thread);

// 销毁线程池
void destory_threadpool(ThreadPool* thread);

#endif//THREADPOOL_H
