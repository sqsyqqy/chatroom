#ifndef NETWORK_H
#define NETWORK_H

#include <stdio.h>
#include <stdbool.h>
#include <netinet/in.h>

typedef struct Network
{
	int type;					// 通信类型 TCP/UDP
	int sock_fd;				// socket描述符
	struct sockaddr_in addr;	// 通信地址结构体
	socklen_t addrlen;			// 地址结构体字节数
	bool issvr;				// 服务端为true 客户端为false
}Network;

typedef struct sockaddr* SP;

// 创建socket对象，初始化地址，绑定，监听，连接
Network* init_nw(int type,short port,const char* ip,bool issvr);

// 等待连接，只有type是SOCK_STREAM时，且是服务端才能调用此函数
Network* accept_nw(Network* srv_nw);

// 具有send和sendto功能的发送函数
ssize_t send_nw(Network* nw,const void* buf,size_t len);

// 具有recv和recvfrom功能的接收函数
ssize_t recv_nw(Network* nw,void* buf,size_t len);

// 关闭socket对象和释放内存
void close_nw(Network* nw);

#endif//NETWORK_H
