#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "network.h"

// 创建socket对象，初始化地址，绑定，监听，连接
Network* init_nw(int type,short port,const char* ip,bool issvr)
{
	Network* nw = malloc(sizeof(Network));

	nw->sock_fd = socket(AF_INET,type,0);
	if(0 > nw->sock_fd)
	{
		free(nw);
		perror("socket");
		return NULL;
	}

	// 计算地址结构体的字节数
	nw->addrlen = sizeof(struct sockaddr_in);

	// 初始化地址结构体
	bzero(&nw->addr,nw->addrlen);

	// 准备地址结构体
	nw->addr.sin_family = AF_INET;
	nw->addr.sin_port = htons(port);
	nw->addr.sin_addr.s_addr = inet_addr(ip);
	
	nw->type = type;
	nw->issvr = issvr;

	// 是服务端
	if(nw->issvr)
	{
		if(bind(nw->sock_fd,(SP)&nw->addr,nw->addrlen))
		{
			free(nw);
			perror("bind");
			return NULL;
		}
		// TCP服务端
		if(SOCK_STREAM == nw->type && listen(nw->sock_fd,50))
		{
			free(nw);
			perror("listen");
			return NULL;
		}
	}
	// 是客户端，如果还是TCP
	else if(SOCK_STREAM == nw->type)
	{
		if(connect(nw->sock_fd,(SP)&nw->addr,nw->addrlen))
		{
			free(nw);
			perror("connect");
			return NULL;
		}
	}
	return nw;
}

// 等待连接，只有type是SOCK_STREAM时，且是服务端才能调用此函数
Network* accept_nw(Network* srv_nw)
{
	if(SOCK_STREAM != srv_nw->type || !srv_nw->issvr)
	{
		printf("只有type是SOCK_STREAM,且issvr为真时，Network对象才能调用此函数\n");
		return NULL;
	}

	Network* nw = malloc(sizeof(Network));
	nw->addrlen = srv_nw->addrlen;
	nw->type = SOCK_STREAM;
	nw->issvr = true;
	nw->sock_fd = accept(srv_nw->sock_fd,(SP)&nw->addr,&nw->addrlen);
	if(0 > nw->sock_fd)
	{
		free(nw);
		perror("accept");
		return NULL;
	}
	return nw;
}

// 具有send和sendto功能的发送函数
ssize_t send_nw(Network* nw,const void* buf,size_t len)
{
	if(SOCK_STREAM == nw->type)
		return send(nw->sock_fd,buf,len,0);
	else
		return sendto(nw->sock_fd,buf,len,0,(SP)&nw->addr,nw->addrlen);
}

// 具有recv和recvfrom功能的接收函数
ssize_t recv_nw(Network* nw,void* buf,size_t len)
{
	if(SOCK_STREAM == nw->type)
		return recv(nw->sock_fd,buf,len,0);
	else
		return recvfrom(nw->sock_fd,buf,len,0,(SP)&nw->addr,&nw->addrlen);
}

// 关闭socket对象和释放内存
void close_nw(Network* nw)
{
	close(nw->sock_fd);
	free(nw);
}
