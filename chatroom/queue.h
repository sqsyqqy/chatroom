#ifndef QUEUE_H
#define QUEUE_H

#include <stdbool.h>

typedef struct Queue
{
	void** arr;  //仓库队列内存首地址
	int cal;	//容量
	int front;  //队头位置
	int rear;   //队尾位置
}Queue;

//创建队列
Queue* create_queue(int cal);

//销毁队列
void destory_queue(Queue* queue);

//入队
void push_queue(Queue* queue,void* arg);

//出队
void pop_queue(Queue* queue);

//队空
bool empty_queue(Queue* queue);

//队满
bool full_queue(Queue* queue);

//队头
void* front_queue(Queue* queue);

//队尾
void* rear_queue(Queue* queue);

#endif //QUEUE_H
