#include <stdlib.h>
#include "threadpool.h"

// 线程中消费线程的入口函数，而不是真正的业务逻辑函数
static void* run(void* arg)
{
	ThreadPool* thread = arg;
	for(;;)
	{
		//消费数据
		void* data = pop_threadpool(thread);
		thread->enter(data);
	}
}

// 创建线程池
ThreadPool* create_threadpool(int thread_cnt,int store_cnt,EnterFP enter)
{
	ThreadPool* thread = malloc(sizeof(ThreadPool));
	thread->tids = malloc(sizeof(pthread_t)*thread_cnt);
	//创建仓库
	thread->store = create_queue(store_cnt);
	thread->thread_cnt = thread_cnt;
	//初始化互斥量、条件变量
	pthread_mutex_init(&thread->flock,NULL);
	pthread_mutex_init(&thread->rlock,NULL);
	pthread_cond_init(&thread->empty,NULL);
	pthread_cond_init(&thread->full,NULL);
	//初始化线程业务逻辑函数
	thread->enter = enter;
	
	return thread;
}

// 启动线程池
void start_threadpool(ThreadPool* thread)
{
	for(int i=0;i<thread->thread_cnt;i++)
	{
		pthread_create(thread->tids+i,NULL,run,thread);
	}
}

// 生产数据
void push_threadpool(ThreadPool* thread,void* data)
{
	// 队尾加锁
	pthread_mutex_lock(&thread->rlock);
	while(full_queue(thread->store))
	{
		//唤醒一个消费线程
		pthread_cond_signal(&thread->empty);
		//睡眠并解锁队尾
		pthread_cond_wait(&thread->full,&thread->rlock);
	}
	//把数据入队
	push_queue(thread->store,data);
	pthread_cond_signal(&thread->empty);
	// 队尾解锁
	pthread_mutex_unlock(&thread->rlock);
}

// 消费数据
void* pop_threadpool(ThreadPool* thread)
{	
	// 队头加锁
	pthread_mutex_lock(&thread->flock);
	while(empty_queue(thread->store))
	{
		//唤醒一个生产线程
		pthread_cond_signal(&thread->full);
		//睡眠并解锁队尾
		pthread_cond_wait(&thread->empty,&thread->flock);
	}
	//消费数据
	void* data = front_queue(thread->store);
	pop_queue(thread->store);
	pthread_cond_signal(&thread->full);
	// 队头解锁
	pthread_mutex_unlock(&thread->flock);
	return data;
}

// 销毁线程池
void destory_threadpool(ThreadPool* thread)
{
	pthread_mutex_destroy(&thread->flock);	
	pthread_mutex_destroy(&thread->rlock);
	pthread_cond_destroy(&thread->empty);
	pthread_cond_destroy(&thread->full);
	destory_queue(thread->store);
	free(thread->tids);
	free(thread);
}

