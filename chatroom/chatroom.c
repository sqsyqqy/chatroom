#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "threadpool.h"
#include "network.h"

#define BUF_SIZE 100
#define CHAT_SIZE 10
#define STORE_SIZE 10
#define NAME_SIZE 100

char Buf[BUF_SIZE];
char Name[CHAT_SIZE][NAME_SIZE];
int Cli_fds[CHAT_SIZE];

void send_all(NetWork* nw,const char* msg)
{
	for(int i=0;i<CHAT_SIZE;i++)
	{
		if(Cli_fds[i] != 0 && Cli_fds[i] != nw->sock_fd)
		{
			send(Cli_fds[i],msg,strlen(msg)+1,0);
		}
	}
}

void enter(void* arg)
{
	NetWork* cli_nw = (NetWork*)arg;
	int exit = 0;
	char name[NAME_SIZE+1];
	int i;
	char buf[BUF_SIZE];
	int ret = recv(cli_nw->sock_fd,name,NAME_SIZE+1,0);
	if(0 >= ret) 
	{
		for(i=0;i<CHAT_SIZE;i++)
		{
			if(Cli_fds[i] == cli_nw->sock_fd)
			{
				Cli_fds[i] = 0;
				break;
			}
		}
		return;
	}
	sprintf(Buf,"%s加入聊天",name);
	send_all(cli_nw,Buf);
	for(i=0;i<CHAT_SIZE;i++)
	{
		if(Cli_fds[i] == cli_nw->sock_fd)
		{
			strcpy(Name[i],name);
			break;
		}
	}
	if(i >= CHAT_SIZE) 
	{
		close_nw(cli_nw);
		return;
	}
	for(;;)
	{
		ret = recv(cli_nw->sock_fd,Buf,BUF_SIZE,0);
		if(0 >=ret ||0 == strcmp(Buf,"quit"))
		{
			sprintf(Buf,"%s退出聊天",name);
			send_all(cli_nw,Buf);
			Cli_fds[i] = 0;
			close_nw(cli_nw);
			break;
		}
		strcpy(buf,Buf);
		sprintf(Buf,"%s：%s",name,buf);
		send_all(cli_nw,Buf);
	}
}

int main(int argc,const char* argv[])
{
	NetWork* nw = init_nw(SOCK_STREAM,6661,"172.20.10.2",true);
	ThreadPool* thread = create_threadpool(CHAT_SIZE,STORE_SIZE,enter);
	start_threadpool(thread);	
	for(;;)
	{	
		int i;
		NetWork* cli_nw = malloc(sizeof(NetWork));
		cli_nw = accept_nw(nw);
		for(i=0;Cli_fds[i%CHAT_SIZE] != 0;i++);
		Cli_fds[i%CHAT_SIZE] = cli_nw->sock_fd;
		push_threadpool(thread,cli_nw);
	}	
}
