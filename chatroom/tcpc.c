#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<string.h>
#include<pthread.h>

typedef struct sockaddr* SP;

void* send2Server(void* arg)
{
	int sockfd = *(int *)arg;
	char buf[4096];
	size_t buf_size = sizeof(buf);
	for(;;)
	{
		gets(buf);
		//发送请求
		int ret = send(sockfd,buf,strlen(buf)+1,0);
		if(0 >= ret)
		{
			printf("服务器维护中...\n");
			close(sockfd);
			exit(0);
			break;
		}
		if(0 == strcmp(buf,"quit"))
		{
			printf("退出\n");
			close(sockfd);
			exit(0);
		}
	}
}

void* recv2Server(void* arg)
{
	int sockfd = *(int *)arg;
	char buf[4096];
	size_t buf_size = sizeof(buf);
	for(;;)
	{	
		int ret = recv(sockfd,buf,buf_size,0);
		if(0 >= ret)
		{
			printf("服务器维护中...\n");
			close(sockfd);
			exit(0);
		}
		printf("%s\n",buf);
	}
}

int main(int argc,const char* argv[])
{
	//创建socket
	int sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(0 > sockfd)
	{
		perror("socket");
		return EXIT_FAILURE;
	}

	//准备通信地址
	struct sockaddr_in addr = {};
	addr.sin_family = AF_INET;
	addr.sin_port = htons(6661);
	addr.sin_addr.s_addr = inet_addr("172.20.10.2");
	socklen_t addrlen = sizeof(addr);

	//连接
	if(connect(sockfd,(SP)&addr,addrlen))
	{
		perror("connect");
		return EXIT_FAILURE;
	}
	char name[20] = {}; 
	printf("请输入你的名字：");
	scanf("%s",name);
	int ret = send(sockfd,name,strlen(name)+1,0);
	if(0 >= ret)
	{
		printf("服务器维护中...\n");
	}
	char buf[4096];
	size_t buf_size = sizeof(buf);
	pthread_t tid_send,tid_recv;
	pthread_create(&tid_send,NULL,send2Server,&sockfd);
	pthread_create(&tid_recv,NULL,recv2Server,&sockfd);
	for(;;);
	close(sockfd);
}
